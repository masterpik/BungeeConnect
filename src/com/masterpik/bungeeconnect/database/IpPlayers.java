package com.masterpik.bungeeconnect.database;

import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeeconnect.Main;
import com.masterpik.bungeeconnect.objects.IP;
import com.masterpik.bungeeconnect.objects.Player;
import com.masterpik.database.api.Query;
import net.md_5.bungee.api.ProxyServer;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class IpPlayers {

    public static HashMap<String, IP> ips;
    public static HashMap<UUID, Player> players;

    public static void IpPlayersInit() {
        ips = new HashMap<>();
        players = new HashMap<>();

        IpPlayers.getInDb();
    }

    public static void getInDb() {

        String query1 = "SELECT * FROM masterpik.ip ORDER BY amount DESC";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            ResultSetMetaData metaData1 = result1.getMetaData();

            while(result1.next()) {

                int ipCompressed = result1.getInt("ip");
                String ip = UtilNetwork.longCompressed2Ip(ipCompressed);

                UUID uuid = (UUID) result1.getObject("uuid");

                int amount = result1.getInt("amount");

                IP ipObject;
                if (!ips.containsKey(ip)) {
                    ipObject = new IP(ip, ipCompressed, false);
                    ips.put(ip, ipObject);
                } else {
                    ipObject = ips.get(ip);
                }

                Player player;
                if (!players.containsKey(uuid)) {
                    player = new Player(uuid);
                    players.put(uuid, player);
                } else {
                    player = players.get(uuid);
                }


                if (!ipObject.getPlayers().containsKey(uuid)) {
                    ipObject.getPlayers().put(uuid, amount);
                }
                if (!player.getIps().containsKey(ip)) {
                    player.getIps().put(ip, amount);
                }

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static String getPlayersFromIp(String stringIp){
        String players = "";

        if (IpPlayers.ips.containsKey(stringIp)) {
            HashMap<UUID, Integer> map = IpPlayers.ips.get(stringIp).getPlayers();

            int bucle = 0;

            while (bucle < map.size()) {


                players = players + UUIDName.uuidToPlayerName((UUID) map.keySet().toArray()[bucle]) + " " + map.get((UUID) map.keySet().toArray()[bucle]);

                if (bucle != map.size() - 1) {
                    players = players + " | ";
                }

                bucle++;
            }
        }

        return players;
    }
}
