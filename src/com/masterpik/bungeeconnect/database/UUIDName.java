package com.masterpik.bungeeconnect.database;

import com.masterpik.api.util.UtilMap;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeeconnect.Main;
import com.masterpik.bungeeconnect.objects.IP;
import com.masterpik.bungeeconnect.objects.Player;
import com.masterpik.database.api.Query;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class UUIDName {

    public static HashMap<UUID, String> uuidTable;

    public static void UUIDNameInit() {
        uuidTable = new HashMap<>();
        getTable();

    }

    public static void getTable() {
        String query1 = "SELECT * FROM masterpik.uuid";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {

                uuidTable.put((UUID) result1.getObject("uuid"), result1.getString("name"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static String uuidToPlayerName(UUID uuid) {

        String name = null;

        if (uuidTable.containsKey(uuid)) {
            name = uuidTable.get(uuid);
        }

        /*String query1 = "SELECT name FROM masterpik.uuid WHERE uuid = '"+uuid+"'";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            result1.next();
            name = result1.getString("name");

        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        return name;
    }

    public static UUID playerNameToUUID(String name) {

        UUID uuid = null;

        if (uuidTable.containsValue(name)) {
            uuid = UtilMap.getKeyByValue(uuidTable, name);
        }


       /* UUID uuid = null;

        String query1 = "SELECT uuid FROM masterpik.uuid WHERE name = '"+name+"'";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            result1.next();
            uuid = (UUID) result1.getObject("uuid");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return uuid;*/
        return uuid;
    }

}
