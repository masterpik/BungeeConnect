package com.masterpik.bungeeconnect.objects;

import java.util.HashMap;
import java.util.UUID;

public class IP {

    private String ip;
    private int ipCompressed;

    private HashMap<UUID, Integer> players;

    private boolean ban;

    public IP(String ip, int ipCompressed, boolean ban) {
        this.ip = ip;
        this.ipCompressed = ipCompressed;
        this.players = new HashMap<>();
        this.ban = ban;
    }

    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getIpCompressed() {
        return ipCompressed;
    }
    public void setIpCompressed(int ipCompressed) {
        this.ipCompressed = ipCompressed;
    }

    public HashMap<UUID, Integer> getPlayers() {
        return players;
    }
    public void setPlayers(HashMap<UUID, Integer> players) {
        this.players = players;
    }

    public boolean isBan() {
        return ban;
    }
    public void setBan(boolean ban) {
        this.ban = ban;
    }
}
