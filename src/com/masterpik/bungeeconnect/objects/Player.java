package com.masterpik.bungeeconnect.objects;

import com.masterpik.bungeecommands.party.Party;

import java.util.HashMap;
import java.util.UUID;

public class Player {

    private UUID uuid;

    private HashMap<String, Integer> ips;

    public Player(UUID uuid) {
        this.uuid = uuid;
        this.ips = new HashMap<>();

    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public HashMap<String, Integer> getIps() {
        return ips;
    }

    public void setIps(HashMap<String, Integer> ips) {
        this.ips = ips;
    }
}
