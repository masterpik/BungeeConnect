package com.masterpik.bungeeconnect.commands;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.Main;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeeconnect.objects.Player;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.UUID;

public class GetLists extends Command{
    public GetLists(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {


        BungeeLogger log = new BungeeLogger(sender, Colors.LIGHT_PURPLE);

        if (strings != null && strings.length >= 1) {



            if (strings.length >=2
                    && (strings[0].equalsIgnoreCase("getIps") || strings[0].equalsIgnoreCase("getPlayers") || strings[0].equalsIgnoreCase("uuid"))) {

                String arg = strings[1];

                if (strings[0].equalsIgnoreCase("getIps")) {

                    UUID uuid = UUIDName.playerNameToUUID(arg);

                    if (uuid != null
                            && IpPlayers.players.containsKey(uuid)) {

                        log.info("All ips for player "+arg+" :");

                        int bucle = 0;
                        ArrayList<String> ips = new ArrayList<>();
                        ips.addAll(IpPlayers.players.get(uuid).getIps().keySet());
                        while (bucle < ips.size()) {

                            log.info((bucle+1)+"- "+ips.get(bucle)+" ("+IpPlayers.players.get(uuid).getIps().get(ips.get(bucle))+")");

                            bucle ++;
                        }


                    } else {
                        log.info("bgco can't find ips for "+arg);
                    }
                }
                else if (strings[0].equalsIgnoreCase("getPlayers")) {
                    if (UtilNetwork.checkIp(arg)
                            && IpPlayers.ips.containsKey(arg)) {

                        log.info("All players for ip "+arg+ " :");

                        int bucle = 0;
                        ArrayList<UUID> players = new ArrayList<>();
                        players.addAll(IpPlayers.ips.get(arg).getPlayers().keySet());
                        while (bucle < players.size()) {

                            log.info((bucle+1)+"- "+UUIDName.uuidToPlayerName(players.get(bucle))+ " ("+IpPlayers.players.get(players.get(bucle)).getIps().get(arg)+")");

                            bucle ++;
                        }

                    } else {
                        log.info("bgco can't find players for "+arg);
                    }
                } else if (strings[0].equalsIgnoreCase("uuid")) {
                    if (UtilUUID.isAnUUID(arg)
                            && UUIDName.uuidTable.containsKey(UUID.fromString(arg))) {
                        log.info("bgco "+ arg+ " --> "+ UUIDName.uuidToPlayerName(UUID.fromString(arg)));
                    } else if (UUIDName.playerNameToUUID(arg) != null) {
                        log.info("bgco "+UUIDName.playerNameToUUID(arg).toString()+" --> "+ arg);
                    } else {
                        log.info("bgco can't find an uuid or a player for "+arg);
                    }
                }

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {
                    if (strings[1].equalsIgnoreCase("getIps")) {
                        log.info("bgco help (getIps) : \n Description : Get all ip for a player \n Usage : /bgco getIps [player]");
                    } else if (strings[1].equalsIgnoreCase("getPlayers")) {
                        log.info("bgco help (getPlayers) : \n Description : Get all players for a ip \n Usage : /bgco getPlayers [ip]");
                    } else if (strings[1].equalsIgnoreCase("uuid")) {
                        log.info("bgco help (uuid) : \n Description : Get UUID of a player or a player of a UUID \n Usage : /bgco uuid [player|uuid]");
                    }
                    else if (strings[1].equalsIgnoreCase("help")) {
                        log.info("bgco help (help) : \n Description : Get the help for a bgco command \n Usage : /bgco help [command]");
                    } else {
                        log.info(Main.ListHelp);
                    }

                } else {
                    log.info(Main.ListHelp);
                }
            } else {
                log.info(Main.ListHelp);
            }

        } else {
            log.info(Main.ListHelp);
        }

    }
}
