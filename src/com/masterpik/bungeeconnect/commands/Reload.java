package com.masterpik.bungeeconnect.commands;

import com.masterpik.bungeeconnect.Main;
import com.masterpik.bungeeconnect.confy.GeneralConfy;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Reload extends Command {
    public Reload(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        if (!(sender instanceof ProxiedPlayer)) {

            Main.dbConnection = com.masterpik.database.bungee.Main.dbConnection;
            GeneralConfy.reload();

            IpPlayers.IpPlayersInit();
            UUIDName.UUIDNameInit();
        }

    }
}
