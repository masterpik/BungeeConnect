package com.masterpik.bungeeconnect.confy;

import com.masterpik.bungeeconnect.Main;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GeneralConfy {
    public static Configuration generalConfy;
    public static File generalFile;


    public static void GeneralInit() {
        try {
            if (!Main.plugin.getDataFolder().exists()) {
                Main.plugin.getDataFolder().mkdir();
            }

            generalFile = new File(Main.plugin.getDataFolder().getPath(), "general.yml");

            if (!generalFile.exists()) {
                generalFile.createNewFile();
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);

                generalConfy.set("General.pingAnalyse", true);

                GeneralConfy.saveFile(generalConfy, generalFile);
            }
            else {
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean getPingSpy() {
        return generalConfy.getBoolean("General.pingAnalyse");
    }

    public static void saveFile(Configuration file, File fileg) {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(file, fileg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void reload() {
        try {
            generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
