package com.masterpik.bungeeconnect;

import com.masterpik.bungeeconnect.commands.GetLists;
import com.masterpik.bungeeconnect.commands.Reload;
import com.masterpik.bungeeconnect.confy.GeneralConfy;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.sql.Connection;

public class Main extends Plugin{

    public static Plugin plugin;
    public static Connection dbConnection;

    public static String ListHelp;

    public void onEnable() {
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Reload("bcreload"));
        this.getProxy().getPluginManager().registerListener(this, new Events());
        dbConnection = com.masterpik.database.bungee.Main.dbConnection;
        plugin = this;

        ListHelp = "bgco list of commands : \n /bgco getIps [player]\n /bgco getPlayers [ip]\n /bgco uuid [player|uuid]\n /bgco help [command]";

        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GetLists("bgco"));

        GeneralConfy.GeneralInit();

        IpPlayers.IpPlayersInit();
        UUIDName.UUIDNameInit();

    }

}
