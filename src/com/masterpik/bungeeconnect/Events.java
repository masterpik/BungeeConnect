package com.masterpik.bungeeconnect;

import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.bungeeconnect.confy.GeneralConfy;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeeconnect.objects.IP;
import com.masterpik.bungeeconnect.objects.Player;
import com.masterpik.connect.api.PlayersStatistics;
import com.masterpik.database.api.Query;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Events implements Listener {

    @EventHandler
    public void ProxyPingEvent(ProxyPingEvent event) {


        PendingConnection connection = event.getConnection();

        String stringIp = connection.getAddress().toString().split(":")[0];
        stringIp = stringIp.replaceAll("/", "");

        if (/*!stringIp.contains("62.210.136.149") && */!stringIp.contains("127.0.0.1")) {

            if (GeneralConfy.getPingSpy()) {

                int ipCompress = UtilNetwork.ip2longCompressed(stringIp);


                String query = "INSERT INTO masterpik.ping (ip, ping_amount, first_ping, last_ping) VALUES (" + ipCompress + ", 0, NOW(), NOW())";
                //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query);
                boolean ok = Query.queryInput(Main.dbConnection, query);

                String players = "("+IpPlayers.getPlayersFromIp(stringIp)+")";

                if (!players.equalsIgnoreCase("()")) {
                    ProxyServer.getInstance().getLogger().info("PING : " + stringIp + " PLAYERS : " + players);
                }
            }



        }
    }

    @EventHandler
    public void LoginEvent(LoginEvent event) {

        PendingConnection connection = event.getConnection();
        String stringIp = connection.getAddress().toString().split(":")[0];
        stringIp = stringIp.replaceAll("/", "");
        UUID uuid = connection.getUniqueId();


        int ipCompress = UtilNetwork.ip2longCompressed(stringIp);

        //ip
        String query = "INSERT INTO masterpik.ip (ip, uuid) VALUES (" + ipCompress + ", '"+ uuid+"')";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query);
        boolean ok = Query.queryInput(Main.dbConnection, query);
        if (!IpPlayers.ips.containsKey(stringIp)) {
            IP ipObject = new IP(stringIp, ipCompress, false);
            IpPlayers.ips.put(stringIp, ipObject);
        }

        //players
        String query2 = "INSERT INTO masterpik.players (uuid, status, party) VALUES ('" + uuid + "', 1 , -1)";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query2);
        boolean ok1 = Query.queryInput(Main.dbConnection, query2);
        if (!IpPlayers.players.containsKey(uuid)) {
            Player player = new Player(uuid);
            IpPlayers.players.put(uuid, player);
        }

        //uuid
        String query3 = "INSERT INTO masterpik.uuid (uuid, name) VALUES ('" + uuid + "', '"+ event.getConnection().getName()+"')";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query3);
        boolean ok2 = Query.queryInput(Main.dbConnection, query3);
        PlayersStatistics.playerConnexion(uuid);

        int amount1 = 0;
        int amount2 = 0;
        if (IpPlayers.ips.containsKey(stringIp)
                && IpPlayers.ips.get(stringIp).getPlayers() != null
                && IpPlayers.ips.get(stringIp).getPlayers().get(uuid) != null) {
            amount1 = IpPlayers.ips.get(stringIp).getPlayers().get(uuid);
        }
        if (IpPlayers.players.containsKey(uuid)
                && IpPlayers.players.get(uuid).getIps() != null
                && IpPlayers.players.get(uuid).getIps().get(stringIp)!=null) {
            amount2 = IpPlayers.players.get(uuid).getIps().get(stringIp);
        }
        IpPlayers.ips.get(stringIp).getPlayers().put(uuid, amount1 + 1);
        IpPlayers.players.get(uuid).getIps().put(stringIp, amount2 + 1);

        UUIDName.uuidTable.put(uuid, connection.getName());
    }





}
